package store

import (
	"log"
	"sync"
	"time"
)

type KVStore struct {
	mu   *sync.RWMutex
	data map[string]Value
	l    log.Logger
}

type Value struct {
	value      string
	expiration time.Duration
}

func NewStore() KVStore {
	return KVStore{
		mu:   &sync.RWMutex{},
		data: make(map[string]Value),
	}
}

//Get returns the value of the provided keyword if it exists
func (s KVStore) Get(keyword string) string {
	s.mu.RLock()
	defer s.mu.RUnlock()
	res, ok := s.data[keyword]
	if !ok {
		return "(nil)"
	}
	return res.value
}

//Set sets the key - value pair as provided into the function call, previous values get overridden if they existed
func (s KVStore) Set(keyword, value string, options ...string) string {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.data[keyword] = Value{value: value, expiration: time.Duration(0)}
	return "OK"
}

//Set sets the key - value pair as provided into the function call, previous values do not get overridden if they exist
func (s KVStore) SetNX(keyword, value string, options ...string) int {
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, ok := s.data[keyword]; !ok {
		s.data[keyword] = Value{value: value, expiration: time.Duration(0)}
		return 1
	}
	return 0
}

func (s KVStore) Del(keyword string) int {
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, ok := s.data[keyword]; ok {
		delete(s.data, keyword)
		return 1
	}
	return 0
}

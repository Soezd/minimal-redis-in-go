package tokenizer

import (
	"regexp"
	"strings"
)

func Tokenize(in string) []string {
	//Trim at start and end of string
	in = strings.TrimSpace(in)
	//Replace "one or more" space characters with a single whitespace
	space := regexp.MustCompile(`\s+`)
	in = space.ReplaceAllString(in, " ")
	return strings.Split(in, " ")
}

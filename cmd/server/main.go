package main

import (
	"bufio"
	"errors"
	"fmt"
	"gitlab.com/Soezd/minimal-redis-in-go/internal/tokenizer"
	"io"
	"log"
	"net"
	"strconv"
	"strings"

	"gitlab.com/Soezd/minimal-redis-in-go/internal/store"
)

type Store interface {
	Get(string) string
	Set(string, string, ...string) string
	SetNX(string, string, ...string) int
	Del(string) int
}

type wrongNumberOfArguments struct {
	expected int
	got      int
}

var (
	s               Store
	notYetSupported = errors.New("not yet supported")
)

func (err wrongNumberOfArguments) Error() string {
	return fmt.Sprintf("(Error) wrong number of Arguments, expected: %v, got: %v", err.expected, err.got)
}

func main() {
	fmt.Println("Launching server...")

	// listen on all interfaces on port 8081
	listener, err := net.Listen("tcp", ":8081")
	// listener.Close can err, so we wrap the call in a function
	defer func() {
		err := listener.Close()
		if err != nil {
			fmt.Printf("Error while closing TCP connection: %s", err)
		}
	}()
	if err != nil {
		log.Fatalf("Error: net.Listen(): %s", err)
	}

	//Initializing the store
	fmt.Println("Initializing the store")
	s = store.NewStore()
	fmt.Println("Store initialized")

	fmt.Println("Listening on port 8081")

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error: Accept(): %s", err)
			continue
		}

		go listenAndServe(conn)
	}
}

func listenAndServe(conn net.Conn) {
	for {
		// will listen for message to process ending in newline (\n)
		message, err := bufio.NewReader(conn).ReadString('\n')
		if err == io.EOF {
			log.Print("Client disconnected")
			conn.Close()
			return
		} else if err != nil {
			log.Printf("Error: %s", err)
			conn.Close()
			return
		}
		if len(message) == 0 {
			continue
		}
		// output message received
		fmt.Print("Message Received:", message)
		//handle the message
		res := dispatch(tokenizer.Tokenize(message))

		// send new string back to client
		_, err = conn.Write([]byte(normalize(res) + "\n"))
		if err != nil {
			log.Printf("Error on conn.Write(): %s", err)
		}
	}
}

func dispatch(cmd []string) interface{} {
	for i := range cmd {
		cmd[i] = strings.TrimSpace(cmd[i])
	}
	switch strings.ToLower(cmd[0]) {
	case "get":
		err := checkNumberOfArguments(len(cmd), 2)
		if err != nil {
			return err.Error()
		}
		return s.Get(cmd[1])
	case "set":
		err := checkNumberOfArguments(len(cmd), 3)
		if err != nil {
			return err.Error()
		}
		return s.Set(cmd[1], cmd[2])
	case "setnx":
		err := checkNumberOfArguments(len(cmd), 3)
		if err != nil {
			return err.Error()
		}
		return s.SetNX(cmd[1], cmd[2])
	case "del":
		err := checkNumberOfArguments(len(cmd), 2)
		if err != nil {
			return err.Error()
		}
		return s.Del(cmd[1])
	default:
		return notYetSupported.Error()
	}
}

func normalize(i interface{}) string {
	switch i.(type) {
	case string:
		return i.(string)
	case int:
		return "(integer) " + strconv.Itoa(i.(int))
	default:
		return "(nil)"
	}
}
func checkNumberOfArguments(got, expected int) error {
	if got != expected {
		return wrongNumberOfArguments{expected, got}
	}
	return nil
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8081")
	defer conn.Close()
	if err != nil {
		log.Fatalf("Error: net.Dial(): Could not connect to 127.0.0.1 on port 8081: %s", err)
	}
	for {
		// read in input from stdin
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Command to send: ")
		text, _ := reader.ReadString('\n')
		// send to socket
		_, _ = fmt.Fprintf(conn, text+"\n")
		// listen for reply
		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Print("Response from server: " + message)
	}
}
